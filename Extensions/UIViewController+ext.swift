//
//  UIViewController.swift
//  Movies
//
//  Created by Marcin Obolewicz on 11/02/2021.
//

import UIKit

extension String {
    static let empty = ""
}

extension UIViewController {
    func showPopup(title: String, message: String? = .empty) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }
}
