//
//  Image+extension.swift
//  Calculator
//
//  Created by Marcin Obolewicz on 01/12/2021.
//

import SwiftUI

extension Image {
    init(asset: Asset) {
        self.init(asset.rawValue)
    }
}
